# Qvest Interview Challenge
**Frontend challenge - Winter 2022**

## Purpose

The purpose of this challenge is to test your skills in frontend web development

- Coding style and practices
- Markup/styling approach and skills
- Time management and prioritization

## Objective

You are given a detailed design document (see assets below) and tasked with implementing it as a single-page web application. You may break this exercise down as you prefer, here is an example of sub-objectives:

 - Render markup (HTML) and stylesheet (CSS) according to design
 - Populate application with provided mock data (JavaScript)
 - Implement filtering mechanism according to design (JavaScript)

Whether you complete all sub-objectives fully and perfectly is **not crucial to the evaluation**. A solution that is sufficiently complete to display skills with regards to all criteria listed in the purpose section above is likely to pass. 

You may use vanilla js, React, Vue, preact, Svelte or any other dependencies you find suitable. As long as you include configuration files (e.g. package.json, webpack.config) that allow us to easily run the application.  

You are expected to spend **4-6 hours** on this task.

## Assets

 - [Figma design document](https://www.figma.com/file/4kU5PEFQ8HXIzIXv3qqlfC/My-Activity-Interview-challenge)
*(Note: If you log in with your own free Figma account you will get access to all measurements, style details, and the ability to export image assets)*
 - Mock data `tasks.json` available in this repo

## Submission

Push your implementation to a git repository (at GitHub or GitLab) and send a link or invite [niels@qvest.io](mailto:niels@qvest.io) if it is private.
